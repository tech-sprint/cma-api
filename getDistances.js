'use strict';

const express = require('express');
const underscore = require('underscore');
const geocoder = require('geocoder');

function getDistanceFromLatLonInMiles(lat1,lon1,lat2,lon2) {
    let R = 3959; // Radius of the earth in miles
    let dLat = deg2rad(lat2-lat1);  // deg2rad below
    let dLon = deg2rad(lon2-lon1);
    let a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    let d = R * c; // Distance in miles
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
}

module.exports = {
    getLocationLatLong: function(latlong, bankInfo, response) {
        let latlongSplit = latlong.split(',');
        let lat = latlongSplit[0];
        let long = latlongSplit[1];
        bankInfo.forEach((entry) => {
            if (entry.GeographicLocation) {
                let latitude = entry.GeographicLocation.Latitude;
                let longitude = entry.GeographicLocation.Longitude;
                entry.Distance = getDistanceFromLatLonInMiles(lat, long, latitude, longitude);
            }
        });
        let sortedBank = underscore.sortBy(bankInfo, 'Distance');
        let radiusBank = underscore.filter(sortedBank, radius => {return radius.Distance <= 5});
        response.json(radiusBank);
    },
    getLocationTown: function(locationQuery, bankInfo, response) {
        geocoder.geocode(locationQuery, function(err, data) {
            if (err) {
                err = new Error("Something went wrong with our server. Please try again.");
                err.status = 500;
                return next(err);
            }
            
            let townGeoLat = data.results[0].geometry.location.lat;
            let townGeoLong = data.results[0].geometry.location.lng;

            bankInfo.forEach((entry) => {
                if (entry.GeographicLocation) {
                    let latitude = entry.GeographicLocation.Latitude;
                    let longitude = entry.GeographicLocation.Longitude;
                    entry.Distance = getDistanceFromLatLonInMiles(townGeoLat, townGeoLong, latitude, longitude);
                }
            });
            let sortedBank = underscore.sortBy(bankInfo, 'Distance');
            let radiusBank = underscore.filter(sortedBank, radius => {return radius.Distance <= 5});
            response.json(radiusBank);
        });
    }
};