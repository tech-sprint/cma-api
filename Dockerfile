FROM mhart/alpine-node:7
WORKDIR /src
ADD . .
RUN npm install
RUN npm run docs
ENV PORT 3000
EXPOSE 3000
CMD ["node", "app.js"]