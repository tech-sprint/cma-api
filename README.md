## Synopsis

A REST API that provides information on ATMs, branches and personal current accounts for nine different banks/building societies. Information can be obtained for a specific provider and/or branches/ATMs that are within a 5 mile radius of a location specified.

### Providers
1. nationwide
2. barclays
3. natwest
4. hsbc
5. rbs
6. bankofscotland
7. halifax
8. santander
9. lloyds

### ATMs
* **/atms** --> all ATMs for all nine providers
* **/atms?location=Swindon** --> all ATMs that are within a 5 mile radius of location (e.g. Swindon)
* **/atms?latlong=54.432423,1.32423423** --> all ATMs that are within a 5 mile radius of lat & long
* **/atms/nationwide** --> all ATMs for a specific provider (e.g. nationwide)
* **/atms/nationwide?location=Swindon** --> all ATMs for a specific provider that are within a 5 mile radius of location (e.g. Swindon)
* **/atms/nationwide?latlong=54.432423,1.32423423** --> all ATMs for a specific provider that are within a 5 mile radius of lat & long

### Branches
* **/branches** --> all branches for all nine providers
* **/branches?location=Swindon** --> all branches that are within a 5 mile radius of location (e.g. Swindon)
* **/branches?latlong=54.432423,1.32423423** --> all branches that are within a 5 mile radius of lat & long
* **/branches/nationwide** --> all branches for a specific provider (e.g. nationwide)
* **/branches/nationwide?location=Swindon** --> all branches for a specific provider that are within a 5 mile radius of location (e.g. Swindon)
* **/branches/nationwide?latlong=54.432423,1.32423423** --> all branches for a specific provider that are within a 5 mile radius of lat & long

### Personal Current Accounts
* **/personal-current-accounts** --> all personal current accounts for all nine providers
* **/personal-current-accounts/nationwide** --> all personal current accounts for a specific provider (e.g. nationwide)

## Installation

`https://gitlab.com/tech-sprint/cma-api.git`  
`npm install`