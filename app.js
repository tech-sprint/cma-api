'use strict';

const express = require('express');
const app = express();
const atmRouter = require('./routes/atmRouter');
const branchRouter = require('./routes/branchRouter');
const pcaRouter = require('./routes/pcaRouter');
const getAPIs = require('./getAPIs');
const bodyParser = require('body-parser').json;


app.use(bodyParser());

// routers
app.use('/atms', atmRouter);
app.use('/branches', branchRouter);
app.use('/personal-current-accounts', pcaRouter);
app.use('/docs', express.static('doc'));

// catch 404 and forward to error handler
app.use((req, res, next) => {
    let err = new Error("Not Found");
    err.status = 404;
    next(err);
});

// error handler
app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        error: err.message
    });
});

// initialise the api data
getAPIs.refreshData();

// refresh api day by a given time interval
setInterval(getAPIs.refreshData, 24*60*60*1000);

// port
const port = process.env.PORT || 4000;

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});