const request = require("request-promise");

function geocode(loc) {
    const options = {sensor: false, address: loc};
    const uri = "https://maps.googleapis.com/maps/api/geocode/json";
    return request({uri: uri, qs: options, json: true})
        .then(data => {
            if(data.status === 'OVER_QUERY_LIMIT'){
                throw new Error('OverLimit');
            }
            return data;
        })
}

module.exports = {
    geocode
};