'use strict';

const express = require('express');
const request = require('request-promise');
const underscore = require('underscore');
const geocoder = require('./geocode');

let atmData;
let branchData;
let pcaData;

// api urls and bank names for atms, branches and personal current accounts
const atmBanks = [
    ['https://api.hsbc.com/open-banking/v1.2/atms', 'hsbc'],
    ['https://atlas.api.barclays/open-banking/v1.3/atms', 'barclays'],
    ['https://openapi.nationwide.co.uk/open-banking/v1.2/atms', 'nationwide'],
    ['https://api.bankofscotland.co.uk/open-banking/v1.2/atms', 'bankofscotland'],
    ['https://api.halifax.co.uk/open-banking/v1.2/atms', 'halifax'],
    ['https://api.lloydsbank.com/open-banking/v1.2/atms', 'lloyds'],
    ['https://openapi.natwest.com/open-banking/v1.2/atms', 'natwest'],
    ['https://openapi.rbs.co.uk/open-banking/v1.2/atms', 'rbs'],
    ['https://api.santander.co.uk/retail/open-banking/v1.2/atms', 'santander']
];

const branchBanks = [
    ['https://api.hsbc.com/open-banking/v1.2/branches', 'hsbc'],
    ['https://atlas.api.barclays/open-banking/v1.3/branches', 'barclays'],
    ['https://openapi.nationwide.co.uk/open-banking/v1.2/branches', 'nationwide'],
    ['https://api.bankofscotland.co.uk/open-banking/v1.2/branches', 'bankofscotland'],
    ['https://api.halifax.co.uk/open-banking/v1.2/branches', 'halifax'],
    ['https://api.lloydsbank.com/open-banking/v1.2/branches', 'lloyds'],
    ['https://openapi.natwest.com/open-banking/v1.2/branches', 'natwest'],
    ['https://openapi.rbs.co.uk/open-banking/v1.2/branches', 'rbs'],
    ['https://api.santander.co.uk/retail/open-banking/v1.2/branches', 'santander']
];

const pcaBanks = [
    ['https://api.hsbc.com/open-banking/v1.2/personal-current-accounts', 'hsbc'],
    ['https://atlas.api.barclays/open-banking/v1.3/personal-current-accounts', 'barclays'],
    ['https://openapi.nationwide.co.uk/open-banking/v1.2/personal-current-accounts', 'nationwide'],
    ['https://api.bankofscotland.co.uk/open-banking/v1.2/personal-current-accounts', 'bankofscotland'],
    ['https://api.halifax.co.uk/open-banking/v1.2/personal-current-accounts', 'halifax'],
    ['https://api.lloydsbank.com/open-banking/v1.2/personal-current-accounts', 'lloyds'],
    ['https://openapi.natwest.com/open-banking/v1.2/personal-current-accounts', 'natwest'],
    ['https://openapi.rbs.co.uk/open-banking/v1.2/personal-current-accounts', 'rbs'],
    ['https://api.santander.co.uk/retail/open-banking/v1.2/personal-current-accounts', 'santander']
];

// the api requests for atms, branches and personal bank accounts
function dataRequest() {
    Promise.all(atmBanks.map((atmBanksRequest) => {
        return request(atmBanksRequest[0], {json: true, "rejectUnauthorized": false})
            .then(body => {
                if (body.data) {
                    return body.data.map(atm => {
                        // add bank name to each object
                        return Object.assign({BankName: atmBanksRequest[1]}, atm);
                    });
                } else {
                    console.log('No ATM data was retrieved.');
                } 
            });
    }))
        .then(body => {
            // remove outer array
            atmData = underscore.flatten(body);
            console.log('atm request successfully made');
        })
        .catch(err => {
            console.log(err);
        });


    Promise.all(branchBanks.map((branchBanksRequest) => {
        return request(branchBanksRequest[0], {json: true, "rejectUnauthorized": false})
            .then(body => {
                if (body.data) {
                    return body.data.map(branch => {
                        // add bank name to each object
                        return Object.assign({BankName: branchBanksRequest[1]}, branch);
                    });
                } else {
                    console.log('No Branch data was retrieved.');
                } 
            });
    }))
        .then(body => {
            // remove outer array
            branchData = underscore.flatten(body);
            console.log('branch request successfully made');
        })
        .catch(err => {
            console.log(err);
        });


    Promise.all(pcaBanks.map((pcaBanksRequest) => {
        return request(pcaBanksRequest[0], {json: true, "rejectUnauthorized": false})
            .then(body => {
                if (body.data) {
                    return body.data.map(pca => {
                        // add bank name to each object
                        return Object.assign({BankName: pcaBanksRequest[1]}, pca);
                    });
                } else {
                    console.log('No Personal Current Account data was retrieved.');
                }
            });
    }))
        .then(body => {
            // remove outer array
            pcaData = underscore.flatten(body);
            console.log('personal current account request successfully made');
        })
        .catch(err => {
            console.log(err);
        });
}

module.exports = {
    refreshData() {
        dataRequest();
    },
    getData() {
        return {atmData, branchData, pcaData}
    }
};