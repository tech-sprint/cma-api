'use strict';

const express = require('express');
const router = express.Router();
const getAPIs = require('../getAPIs');
const underscore = require('underscore');
const getLocation = require('../getDistances');

/**
 * @api {get} /branches All
 * @apiName GetBranches
 * @apiGroup Branches
 * @apiVersion 1.0.0
 * @apiDescription Outputs all the branches for all the providers (nine in total). A location query string is optional and formatted: ?location=Swindon. Using the location query lists the branches within an 5 mile radius of the location selected (closest first). Location can be a town or post code. Also, location can be determined by lat and long values: ?latlong=54.42434,0.432334.
 * @apiParam {string} [location] (Query String) - Search branches by location - e.g. /branches?location=Swindon
 * @apiParam {string} [latlong] (Query String) - /branches?latlong=54.3423432,-2.43242342
 * @apiSampleRequest /branches
 * @apiExample Example - All Branches:
 * https://cma-api.innovationwide.co.uk/branches
 * @apiExample Example - All Branches by location:
 * https://cma-api.innovationwide.co.uk/branches?location=Swindon
 * @apiExample Example - All Branches by lat/long:
 * https://cma-api.innovationwide.co.uk/branches?latlong=51.555774,-1.779718
 * @apiSuccessExample {json} Success Example
 *  [
 *      {
          "Organisation": {
            "ParentOrganisation": {
              "LEI": "PTCQB104N23FMNK2RZ28",
              "BIC": "ABBYGB2LXXX",
              "OrganisationName": {
                "LegalName": "Santander UK PLC"
              }
            },
            "Brand": {
              "TrademarkIPOCode": "EU",
              "TrademarkID": "9415605"
            }
          },
          "BranchName": "Leeds PR",
          "BranchIdentification": "26",
          "BranchType": "Physical",
          "Address": {
            "StreetName": "10, Park Row",
            "BuildingNumberOrName": "10",
            "PostCode": "LS1 5HD",
            "TownName": "Leeds",
            "CountrySubDivision": "West Yorkshire",
            "Country": "GB"
          },
          "BranchDescription": "Retail Branch",
          "TelephoneNumber": "+44-8000284157",
          "GeographicLocation": {
            "Latitude": "53.79833326",
            "Longitude": "-1.54667558"
          },
          "OpeningTimes": [
            {
              "OpeningDay": "Monday",
              "OpeningTime": "10:00:00.000",
              "ClosingTime": "16:00:00.000",
              "UnavailableStartTime": "16:01:00.000",
              "UnavailableFinishTime": "9:59:00.000"
            },
            {
              "OpeningDay": "Tuesday",
              "OpeningTime": "10:00:00.000",
              "ClosingTime": "16:00:00.000",
              "UnavailableStartTime": "16:01:00.000",
              "UnavailableFinishTime": "9:59:00.000"
            },
            {
              "OpeningDay": "Wednesday",
              "OpeningTime": "10:00:00.000",
              "ClosingTime": "16:00:00.000",
              "UnavailableStartTime": "16:01:00.000",
              "UnavailableFinishTime": "9:59:00.000"
            },
            {
              "OpeningDay": "Thursday",
              "OpeningTime": "10:00:00.000",
              "ClosingTime": "16:00:00.000",
              "UnavailableStartTime": "16:01:00.000",
              "UnavailableFinishTime": "9:59:00.000"
            },
            {
              "OpeningDay": "Friday",
              "OpeningTime": "10:00:00.000",
              "ClosingTime": "16:00:00.000",
              "UnavailableStartTime": "16:01:00.000",
              "UnavailableFinishTime": "9:59:00.000"
            }
          ],
          "BranchFacilitiesName": [
            "Wifi"
          ],
          "CustomerSegment": [
            "Personal",
            "Business",
            "Premier",
            "SME"
          ],
          "ATMAtBranch": true,
          "BankName": "santander"
        },
 *  ]
 */

// GET /branches
// gets list of json data for all banks in the branch section
router.get('/', (req, res) => {
    let branchData = getAPIs.getData().branchData;

    if (req.query.location) {
        getLocation.getLocationTown(req.query.location, branchData, res);
    }  else if (req.query.latlong) {
        getLocation.getLocationLatLong(req.query.latlong, branchData, res);
    } else {
      if (branchData.length > 0) {
        res.json(branchData);
      } else {
        let err = new Error("Branch Data Not Found");
        err.status = 404;
        return next(err);
      }
    }
});

/**
 * @api {get} /branches/:provider By Provider
 * @apiName Get Branches by Provider
 * @apiGroup Branches
 * @apiVersion 1.0.0
 * @apiParam {string} provider (URL Parameter) - Search Branches by provider e.g. /branches/nationwide
 * @apiParam {string} [location] (Query String) - Search Branches by provider and location - e.g. /branches/nationwide?location=Swindon
 * @apiParam {string} [latlong] (Query String) - /branches/nationwide?latlong=54.3423432,-2.43242342
 * @apiDescription Outputs all the branches for a specific provider. A location query string is optional and formatted: ?location=Swindon. Using the location query lists the branches (from chosen provider) within an 5 mile radius of the location selected (closest first). Location can be a town or post code. Also, location can be determined by lat and long values: ?latlong=54.42434,0.432334. There are nine providers to choose from, formatted as follows: 
 * 1. barclays 
 * 2. nationwide 
 * 3. hsbc 
 * 4. halifax 
 * 5. santander 
 * 6. natwest 
 * 7. bankofscotland 
 * 8. rbs 
 * 9. lloyds. 
 * @apiSampleRequest /branches/:provider
 * @apiExample Example - All Branches for a provider:
 * https://cma-api.innovationwide.co.uk/branches/nationwide
 * @apiExample Example - All Branches for a provider by location:
 * https://cma-api.innovationwide.co.uk/branches/nationwide?location=Swindon
 * @apiExample Example - All Branches for a provider by lat/long:
 * https://cma-api.innovationwide.co.uk/branches/nationwide?latlong=51.555774,-1.779718
 * @apiError (Error 404) ProviderNotFound The <code>provider</code> was not found
 * @apiSuccessExample {json} Success
 *  [
 *      {
          "Organisation": {
            "ParentOrganisation": {
              "LEI": "PTCQB104N23FMNK2RZ28",
              "BIC": "ABBYGB2LXXX",
              "OrganisationName": {
                "LegalName": "Santander UK PLC"
              }
            },
            "Brand": {
              "TrademarkIPOCode": "EU",
              "TrademarkID": "9415605"
            }
          },
          "BranchName": "Leeds PR",
          "BranchIdentification": "26",
          "BranchType": "Physical",
          "Address": {
            "StreetName": "10, Park Row",
            "BuildingNumberOrName": "10",
            "PostCode": "LS1 5HD",
            "TownName": "Leeds",
            "CountrySubDivision": "West Yorkshire",
            "Country": "GB"
          },
          "BranchDescription": "Retail Branch",
          "TelephoneNumber": "+44-8000284157",
          "GeographicLocation": {
            "Latitude": "53.79833326",
            "Longitude": "-1.54667558"
          },
          "OpeningTimes": [
            {
              "OpeningDay": "Monday",
              "OpeningTime": "10:00:00.000",
              "ClosingTime": "16:00:00.000",
              "UnavailableStartTime": "16:01:00.000",
              "UnavailableFinishTime": "9:59:00.000"
            },
            {
              "OpeningDay": "Tuesday",
              "OpeningTime": "10:00:00.000",
              "ClosingTime": "16:00:00.000",
              "UnavailableStartTime": "16:01:00.000",
              "UnavailableFinishTime": "9:59:00.000"
            },
            {
              "OpeningDay": "Wednesday",
              "OpeningTime": "10:00:00.000",
              "ClosingTime": "16:00:00.000",
              "UnavailableStartTime": "16:01:00.000",
              "UnavailableFinishTime": "9:59:00.000"
            },
            {
              "OpeningDay": "Thursday",
              "OpeningTime": "10:00:00.000",
              "ClosingTime": "16:00:00.000",
              "UnavailableStartTime": "16:01:00.000",
              "UnavailableFinishTime": "9:59:00.000"
            },
            {
              "OpeningDay": "Friday",
              "OpeningTime": "10:00:00.000",
              "ClosingTime": "16:00:00.000",
              "UnavailableStartTime": "16:01:00.000",
              "UnavailableFinishTime": "9:59:00.000"
            }
          ],
          "BranchFacilitiesName": [
            "Wifi"
          ],
          "CustomerSegment": [
            "Personal",
            "Business",
            "Premier",
            "SME"
          ],
          "ATMAtBranch": true,
          "BankName": "santander",
          "Distance": 56.44353453
        },
 *  ]
 * @apiErrorExample Incorrect Provider Name Response (404):
 * HTTP 1.1/ 404 Not Found
 * {
  "error": "ProviderNotFound"
}
 */

// GET /branches/:bankName
// gets list of json data for a specific bank in the branch section
router.get('/:bankName?', (req, res, next) => {
    let branchData = getAPIs.getData().branchData;

    // filter branches by bank name
    let bank = underscore.filter(branchData, function(entry) {return entry.BankName === req.params.bankName;});

    if (req.query.location) {
        getLocation.getLocationTown(req.query.location, bank, res);
    } else if (req.query.latlong) {
        getLocation.getLocationLatLong(req.query.latlong, bank, res);
    } else {
        if (bank.length > 0) {
            res.json(bank);
        } else {
          let err = new Error("ProviderNotFound");
          err.status = 404;
          return next(err);
        }
    }
});


module.exports = router;
