'use strict';

const express = require('express');
const router = express.Router();
const getAPIs = require('../getAPIs');
const underscore = require('underscore');

/**
 * @api {get} /personal-current-accounts All
 * @apiName Get Personal Current Accounts
 * @apiGroup Personal Current Accounts
 * @apiVersion 1.0.0
 * @apiDescription Outputs all personal current accounts for all providers (nine in total).
 * @apiSampleRequest /personal-current-accounts
 * @apiExample Example - All Personal Current Accounts:
 * https://cma-api.innovationwide.co.uk/personal-current-accounts
 * @apiSuccessExample {json} Success Example
 *      [
 *          {
              "Organisation": {
                "ParentOrganisation": {
                  "LEI": "549300PPXHEU2JF0AM85",
                  "OrganisationName": {
                    "LegalName": "LLOYDS BANKING GROUP"
                  }
                },
                "Brand": {
                  "TrademarkIPOCode": "UK",
                  "TrademarkID": "UK00001286876"
                }
              },
              "ProductType": "PCA",
              "ProductName": "Classic Account",
              "ProductSegment": [
                "General"
              ],
              "OverdraftOffered": true,
              "Feature": [
                {
                  "ExistingFeature": true,
                  "ProductSubType": "Regular",
                  "FeatureDetails": [
                    {
                      "FeatureSubType": "Regular",
                      "FeatureName": "Planned Overdraft",
                      "FeatureDescription": "This account comes with up to £25 interest and fee free Planned Overdraft (subject to application and approval)."
                    }
                  ]
                }
              ],
              "Eligibility": {
                "AgeRestricted": true,
                "MinimumAge": 18,
                "OtherFinancialHoldingRequired": false,
                "Description": "you’re aged 18 or older and a resident in the UK\nor have permission to stay in the UK for at least 12 months",
                "IncomeTurnoverRelated": false,
                "ResidencyRestricted": true,
                "ResidencyRestrictedRegion": "UK",
                "ThirdSectorOrganisations": false,
                "PreviousBankruptcy": true
              },
              "BankName": "lloyds"
              ...
 *      ]
 */

// GET /personal-current-accounts
// gets list of json data for all banks in the personal-current-accounts section
router.get('/', (req, res) => {
    let pcaData = getAPIs.getData().pcaData;

    if (pcaData.length > 0) {
      res.json(pcaData);
    } else {
      let err = new Error("Personal Current Account Data Not Found");
      err.status = 404;
      return next(err);
    } 
});

/**
 * @api {get} /personal-current-accounts/:provider By Provider
 * @apiName Get Personal Current Accounts by Provider
 * @apiGroup Personal Current Accounts
 * @apiVersion 1.0.0
 * @apiParam {string} provider (URL Parameter) - Search Personal Current Accounts by provider e.g. /personal-current-accounts/nationwide
 * @apiDescription Outputs all the personal current accounts for a specific provider. There are nine banks to choose from, formatted as follows: 
 * 1. barclays 
 * 2. nationwide 
 * 3. hsbc 
 * 4. halifax 
 * 5. santander 
 * 6. natwest 
 * 7. bankofscotland 
 * 8. rbs 
 * 9. lloyds.
 * @apiSampleRequest /personal-current-accounts/:provider
 * @apiExample Example - All Personal Current Accounts for a provider:
 * https://cma-api.innovationwide.co.uk/personal-current-accounts/nationwide
 * @apiError (Error 404) ProviderNotFound The <code>provider</code> was not found
 * @apiSuccessExample {json} Success Example
 *  [
 *      {
              "Organisation": {
                "ParentOrganisation": {
                  "LEI": "549300PPXHEU2JF0AM85",
                  "OrganisationName": {
                    "LegalName": "LLOYDS BANKING GROUP"
                  }
                },
                "Brand": {
                  "TrademarkIPOCode": "UK",
                  "TrademarkID": "UK00001286876"
                }
              },
              "ProductType": "PCA",
              "ProductName": "Classic Account",
              "ProductSegment": [
                "General"
              ],
              "OverdraftOffered": true,
              "Feature": [
                {
                  "ExistingFeature": true,
                  "ProductSubType": "Regular",
                  "FeatureDetails": [
                    {
                      "FeatureSubType": "Regular",
                      "FeatureName": "Planned Overdraft",
                      "FeatureDescription": "This account comes with up to £25 interest and fee free Planned Overdraft (subject to application and approval)."
                    }
                  ]
                }
              ],
              "Eligibility": {
                "AgeRestricted": true,
                "MinimumAge": 18,
                "OtherFinancialHoldingRequired": false,
                "Description": "you’re aged 18 or older and a resident in the UK\nor have permission to stay in the UK for at least 12 months",
                "IncomeTurnoverRelated": false,
                "ResidencyRestricted": true,
                "ResidencyRestrictedRegion": "UK",
                "ThirdSectorOrganisations": false,
                "PreviousBankruptcy": true
              },
              "BankName": "lloyds"
              ...
 *  ]
 * @apiErrorExample Incorrect Provider Name Response (404):
 * HTTP 1.1/ 404 Not Found
 * {
  "error": "ProviderNotFound"
}
 */

// GET /personal-current-accounts/:bankName
// gets list of json data for a specific bank in the pca section
router.get('/:bankName', (req, res, next) => {
    let pcaData = getAPIs.getData().pcaData;

    // filter branches by bank name
    let bank = underscore.filter(pcaData, function(entry) {return entry.BankName === req.params.bankName;});

    if (bank.length > 0) {
        res.json(bank);
    } else {
        let err = new Error("ProviderNotFound");
        err.status = 404;
        return next(err);
    }
});

module.exports = router;