'use strict';

const express = require('express');
const router = express.Router();
const getAPIs = require('../getAPIs');
const underscore = require('underscore');
const getLocation = require('../getDistances');

/**
 * @api {get} /atms All
 * @apiName GetATMs
 * @apiGroup ATMs
 * @apiVersion 1.0.0
 * @apiDescription Outputs all the ATMs for all the providers (nine in total). A location query string is optional and formatted: ?location=Swindon. Using the location query lists the ATMs within an 5 mile radius of the location selected (closest first). Location can be a town or post code. Also, location can be determined by lat and long values: ?latlong=54.42434,0.432334.
 * @apiParam {string} [location] (Query String) - Search ATMs by location - e.g. /atms?location=Swindon
 * @apiParam {string} [latlong] (Query String) - /atms?latlong=54.3423432,-2.43242342
 * @apiSampleRequest /atms
 * @apiExample Example - All ATMs:
 * https://cma-api.innovationwide.co.uk/atms
 * @apiExample Example - All ATMs by location:
 * https://cma-api.innovationwide.co.uk/atms?location=Swindon
 * @apiExample Example - All ATMs by lat/long:
 * https://cma-api.innovationwide.co.uk/atms?latlong=51.555774,-1.779718
 * @apiSuccessExample {json} Success Example
 *      [
 *          {
              "Organisation": {
                    "ParentOrganisation": {
                      "BIC": "BARCGB22",
                      "OrganisationName": {
                        "LegalName": "Barclays Bank PLC"
                      }
                    },
                    "Brand": {
                      "TrademarkIPOCode": "EU",
                      "TrademarkID": "000055236"
                    }
                  },
                  "ATMID": "UK-B-20512302",
                  "Address": {
                    "StreetName": "Conway Road",
                    "BuildingNumberOrName": "175",
                    "PostCode": "LL31 9EG",
                    "TownName": "Gwynedd",
                    "CountrySubDivision": "Wales",
                    "Country": "GBR"
                  },
                  "GeographicLocation": {
                    "Latitude": "53.28357270",
                    "Longitude": "-3.80332280"
                  },
                  "AccessibilityTypes": [
                    "InductionLoop"
                  ],
                  "SupportedLanguages": [
                    "ENG"
                  ],
                  "ATMServices": [
                    "CashWithdrawal",
                    "Balance",
                    "MiniStatement",
                    "PINChange",
                    "MobilePaymentRegistration",
                    "MobilePhoneTopUp"
                  ],
                  "Currency": [
                    "GBP"
                  ]
                },
                "BankName": "barclays"
            }
 *      ]
 */

// GET /atms
// gets list of json data for all banks in the atm section
router.get('/', (req, res) => {
    let atmData = getAPIs.getData().atmData;

    if (req.query.location) {
        getLocation.getLocationTown(req.query.location, atmData, res);
    } else if (req.query.latlong) {
      getLocation.getLocationLatLong(req.query.latlong, atmData, res);
    } else {
      if (atmData.length > 0) {
        res.json(atmData);
      } else {
        let err = new Error("ATM Data Not Found");
        err.status = 404;
        return next(err);
      }    
    }
});

/**
 * @api {get} /atms/:provider By Provider
 * @apiName Get ATMs by Provider
 * @apiGroup ATMs
 * @apiVersion 1.0.0
 * @apiParam {string} provider (URL Parameter) - Search ATMs by provider e.g. /atms/nationwide
 * @apiParam {string} [location] (Query String) - Search ATMs by provider and location - e.g. /atms/nationwide?location=
 * @apiParam {string} [latlong] (Query String) - /atms/nationwide?latlong=54.3423432,-2.43242342
 * @apiDescription Outputs all the ATMs for a specific provider. A location query string is optional and formatted: ?location=Swindon. Using the location query lists the ATMs (from chosen provider) within an 5 mile radius of the location selected (closest first). Location can be a town or post code. Also, location can be determined by lat and long values: ?latlong=54.42434,0.432334. There are nine providers to choose from, formatted as follows: 
 * 1. barclays 
 * 2. nationwide 
 * 3. hsbc 
 * 4. halifax 
 * 5. santander 
 * 6. natwest 
 * 7. bankofscotland 
 * 8. rbs 
 * 9. lloyds. 
 * @apiSampleRequest /atms/:provider
 * @apiExample Example - All ATMs for a provider:
 * https://cma-api.innovationwide.co.uk/atms/nationwide
 * @apiExample Example - All ATMs for a provider by location:
 * https://cma-api.innovationwide.co.uk/atms/nationwide?location=Swindon
 * @apiExample Example - All ATMs for a provider by lat/long:
 * https://cma-api.innovationwide.co.uk/atms/nationwide?latlong=51.555774,-1.779718
 * @apiError (Error 404) ProviderNotFound The <code>provider</code> was not found
 * @apiSuccessExample {json} Success Example
 *  [
 *          {
              "Organisation": {
                    "ParentOrganisation": {
                      "BIC": "BARCGB22",
                      "OrganisationName": {
                        "LegalName": "Barclays Bank PLC"
                      }
                    },
                    "Brand": {
                      "TrademarkIPOCode": "EU",
                      "TrademarkID": "000055236"
                    }
                  },
                  "ATMID": "UK-B-20512302",
                  "Address": {
                    "StreetName": "Conway Road",
                    "BuildingNumberOrName": "175",
                    "PostCode": "LL31 9EG",
                    "TownName": "Gwynedd",
                    "CountrySubDivision": "Wales",
                    "Country": "GBR"
                  },
                  "GeographicLocation": {
                    "Latitude": "53.28357270",
                    "Longitude": "-3.80332280"
                  },
                  "AccessibilityTypes": [
                    "InductionLoop"
                  ],
                  "SupportedLanguages": [
                    "ENG"
                  ],
                  "ATMServices": [
                    "CashWithdrawal",
                    "Balance",
                    "MiniStatement",
                    "PINChange",
                    "MobilePaymentRegistration",
                    "MobilePhoneTopUp"
                  ],
                  "Currency": [
                    "GBP"
                  ]
                },
                "BankName": "barclays",
                "Distance": 23.43241431231
             }
 *      ]
 * @apiErrorExample Incorrect Provider Name Response (404):
 * HTTP 1.1/ 404 Not Found
 * {
  "error": "ProviderNotFound"
}
 */

// GET /atms/:bankName?
// gets list of json data for a specific bank in the atm section
router.get('/:bankName?', (req, res, next) => {
    let atmData = getAPIs.getData().atmData;

    // filter atms by bank name
    let bank = underscore.filter(atmData, function(entry) {return entry.BankName === req.params.bankName;});

    if (req.query.location) {
        getLocation.getLocationTown(req.query.location, bank, res);
    } else if (req.query.latlong) {
        getLocation.getLocationLatLong(req.query.latlong, bank, res);
    } else {
        if (bank.length > 0) {
            res.json(bank);
        } else {
          let err = new Error("ProviderNotFound");
          err.status = 404;
          return next(err);
        }
    }
});

module.exports = router;